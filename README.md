# Application annuaire de recherche entièrement Kubernetisée

Prérequis :
  - Avoir un cluster Kubernetes fonctionnel
  - Etre capable d'attaquer ce cluster sur votre machine à l'aide de la commande kubectl

Une fois le projet cloné, se déplacer dans le repertoire /kube-iac et saisir la commande suivante dans un Terminal : **kubectl apply -k .** 

L'application est accessible ici : http://annuaire.gkemayo.com/annuaire-ui/#/
