import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackMenuComponent } from './back-menu.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('BackMenuComponent', () => {
  let component: BackMenuComponent;
  let fixture: ComponentFixture<BackMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [ BackMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
