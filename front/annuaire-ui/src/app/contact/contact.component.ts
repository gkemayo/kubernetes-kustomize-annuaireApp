import { Component, OnInit } from '@angular/core';
import { Contact } from "src/app/model/contact";
import { ContactService } from "src/app/service/contact.service";

@Component( {
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.css']
} )
export class ContactComponent implements OnInit {

    public headsTab = ['ID', 'PRENOM', 'NOM', 'EMAIL', 'COMPAGNIE', 'DEPARTEMENT'];
    public types = ['email', 'nom'];
    public isNoResult: boolean = true;
    public isFormSubmitted: boolean = false;
    public displayType: string = 'email';
    public email: string;
    public lastname: string;
    public searchContactResult: Contact[] = [];

    constructor( private contactService: ContactService ) { }

    ngOnInit() {
    }

    /**
     * Recherche les contacts par email ou par nom de famille
     * @param searchContactForm
     */
    searchContactsByType( searchContactForm ) {
        if ( this.displayType === 'email' ) {
            this.searchContactResult = [];
            this.contactService.searchContactByEmail( this.email ).subscribe(
                result => {
                    if ( result && result != null ) {
                        this.searchContactResult.push( result );
                        this.isNoResult = false;
                        return;
                    }
                    this.isNoResult = true;
                },
                error => {
                    console.log( 'An error occurs when searching the contact data : ' + error );
                }
            );
        } else if ( this.displayType === 'nom' ) {
            this.searchContactResult = [];
            this.contactService.searchContactByLastname( this.lastname ).subscribe(
                result => {
                    if ( result && result != null ) {
                        this.searchContactResult = result;
                        this.isNoResult = false;
                        return;
                    }
                    this.isNoResult = true;
                },
                error => {
                    console.log( 'An error occurs when searching the contact data : ' + error );
                }
            );
        }
        this.isFormSubmitted = searchContactForm.submitted;
    }

}
