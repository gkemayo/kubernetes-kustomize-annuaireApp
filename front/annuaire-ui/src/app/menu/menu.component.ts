import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  title = 'Bienvenue dans votre Annuaire !';

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {
  }

  goToSearchContactPage(){
    this.router.navigateByUrl('/contact');
  }

  ngOnInit() {
  }

}
