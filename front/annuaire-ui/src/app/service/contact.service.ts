import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Contact } from "src/app/model/contact";

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(private http: HttpClient) { }

  /**
   * Recherche un contact par son email
   * @param isbn
   */
  searchContactByEmail(email: string): Observable<Contact>{
      return  this.http.get<Contact>('/annuaire/rest/contact/api/search/email?reqParam='+email);
  }

 /**
  * Recherche les contacts correspondant au nom passé en paramètre
  * @param title
  */
  searchContactByLastname(lastname: string): Observable<Contact[]>{
          return this.http.get<Contact[]>('/annuaire/rest/contact/api/search/lastname?reqParam='+lastname);
  }
}
