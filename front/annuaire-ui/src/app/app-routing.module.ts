import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactComponent } from "src/app/contact/contact.component";
import { AppComponent } from "./app.component";
import { MenuComponent } from './menu/menu.component';

const routes: Routes = [
                        {path: '', component: MenuComponent},
                        {path: 'contact', component: ContactComponent}
                        ];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
