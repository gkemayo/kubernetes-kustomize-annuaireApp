// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html
//process.env.CHROME_BIN = require('puppeteer').executablePath();

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-coverage-istanbul-reporter'),
      require('@angular-devkit/build-angular/plugins/karma')
    ],
    client: {
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    coverageIstanbulReporter: {
      //dir: require('path').join(__dirname, '../coverage/annuaire-ui'),
      dir: require('path').join(__dirname, '../coverage'),
      reports: ['html', 'lcovonly', 'text-summary'],
      fixWebpackSourcePaths: true,
      check: {
        global: {
          statements: 50,
          branches: 50,
          functions: 50,
          lines: 50
        }
      }
    },
    reporters: ['progress', 'kjhtml'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    //browsers: ['Chrome'],
    browsers: ['ChromeHeadLeassNoSandbox'],
    customLaunchers: {
      ChromeHeadLeassNoSandbox: {
        base: 'ChromeHeadless',
        browserDisconnectTimeout: 5000,
        browserDisconnectTolerance: 3,
        browserNoActivityTimeout: 30000,
        flags: [
          "--headless",
          "--disable-gpu",
          "--disable-translate",
          "--disable-extensions",
          "--no-sandbox",
        ],
      },
    },
    singleRun: false,
    restartOnFileChange: true
  });
};
