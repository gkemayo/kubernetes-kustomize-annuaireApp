package com.gkemayo.contact.service.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AnnuaryDataAccessException extends RuntimeException {
	
    private static final long serialVersionUID = 1L;
	
	private String errorCode;
	private HttpStatus httpStatus;
	
	public AnnuaryDataAccessException(String errorCode, String message, HttpStatus httpStatus) {
		super(message);
		this.errorCode = errorCode;
		this.httpStatus = httpStatus;
	}


}
