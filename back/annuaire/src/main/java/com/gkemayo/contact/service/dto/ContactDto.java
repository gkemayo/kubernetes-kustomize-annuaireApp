package com.gkemayo.contact.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ContactDto {

	private String id;

	private String firstName;
	
	private String lastName;
	
	private String email;
	
	private String company;
	
	private String departement;
	
}