package com.gkemayo.contact.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gkemayo.contact.repository.entity.Contact;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Integer> {
	
	/**
	 * Retourne un contact dans l'annuaire dont l'email correspond à celui passé en paramètre.
	 * @param email
	 * @return
	 */
	public Contact findByEmailIgnoreCase(String email);
	
	/**
	 * Retourne la liste de tous les contacts dans l'annuaire ayant le nom passé en paramètre
	 * @return
	 */
	public List<Contact> findByLastNameIgnoreCase(String lastName);

}
