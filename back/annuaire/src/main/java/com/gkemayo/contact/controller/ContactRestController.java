package com.gkemayo.contact.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gkemayo.contact.service.ContactService;
import com.gkemayo.contact.service.dto.ContactDto;
import com.gkemayo.contact.service.exception.AnnuaryDataAccessException;
import com.gkemayo.contact.service.exception.ContactNotFoundException;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/rest/contact/api")
@CrossOrigin(origins = "*")
@Slf4j
public class ContactRestController {
	
	@Autowired
	private ContactService contactService;
	
	/**
	 * Retourne un contact dans l'annuaire dont l'email correspond à celui passé en paramètre.
	 * @param email
	 * @return
	 */
	@GetMapping("/search/email")
	public ResponseEntity<ContactDto> findContactByEmail(@RequestParam("reqParam") String email){
		
		log.info("Entering Rest controller /search/email");
		ContactDto contactDto = null;
		try {
			contactDto = contactService.findContactByEmail(email);
		} catch (AnnuaryDataAccessException e) {
			log.error("AnnuaryDataAccessException occurs when calling /search/email {}", e);
			return new ResponseEntity<ContactDto>(e.getHttpStatus());
		} catch(ContactNotFoundException e) {
			log.error("ContactNotFoundException occurs when calling /search/email {}", e);
			return new ResponseEntity<ContactDto>(e.getHttpStatus());
		}catch(Exception e) {
			log.error("An Exception occurs when calling /search/email {}", e);
			return new ResponseEntity<ContactDto>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<ContactDto>(contactDto, HttpStatus.OK);
	}
	
	/**
	 * Retourne la liste de tous les contacts dans l'annuaire ayant le nom passé en paramètre
	 * @return
	 */
	@GetMapping("/search/lastname")
	public ResponseEntity<List<ContactDto>> findByLastName(@RequestParam("reqParam") String lastName){
		
		log.info("Entering Rest controller /search/lastname");
		List<ContactDto> contactDtoList = null;
		try {
			contactDtoList = contactService.findByLastName(lastName);
		} catch (AnnuaryDataAccessException e) {
			log.error("AnnuaryDataAccessException occurs when calling search/lastname {}", e);
			return new ResponseEntity<List<ContactDto>>(e.getHttpStatus());
		} catch(Exception e) {
			log.error("An Exception occurs when calling search/lastname {}", e);
			return new ResponseEntity<List<ContactDto>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<List<ContactDto>>(contactDtoList, HttpStatus.OK);
	}

}
