package com.gkemayo.contact;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.gkemayo.contact.repository.ContactRepository;
import com.gkemayo.contact.repository.entity.Contact;
import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameTranslateMappingStrategy;

@SpringBootApplication
public class AnnuaireApplication implements ApplicationRunner {

	@Autowired
	private ContactRepository contactRepository;
	
	@Value("${path.to.file}")
	private String path;

	public static void main(String[] args) {
		SpringApplication.run(AnnuaireApplication.class, args);
	}

	/**
	 * Charge les données initiales dans la BDD au démarrage de l'application
	 */
	@Override
	public void run(ApplicationArguments args) throws Exception {

		try {

			List<Contact> contactList = csvToBean().parse();
			contactList.forEach(c -> contactRepository.save(c));

		} catch (Exception e) {
			throw new RuntimeException("Impossible to load data while initializing application : " + e);
		}
	}

	/**
	 * Configure et renvoi le parseur de fichier csv
	 * @return
	 * @throws FileNotFoundException
	 */
	private CsvToBean<Contact> csvToBean() throws FileNotFoundException {
		@SuppressWarnings("serial")
		Map<String, String> columnMapping = new HashMap<String, String>() {
			{
				put("id", "id");
				put("first_name", "firstName");
				put("last_name", "lastName");
				put("email", "email");
				put("company", "company");
				put("departement", "departement");
			}
		};

		HeaderColumnNameTranslateMappingStrategy<Contact> mappingStrategy = new HeaderColumnNameTranslateMappingStrategy<Contact>();
		mappingStrategy.setType(Contact.class);
		mappingStrategy.setColumnMapping(columnMapping);

		CsvToBean<Contact> csvToBean = new CsvToBeanBuilder<Contact>(new CSVReader(new FileReader(path)))
				.withType(Contact.class)
				.withMappingStrategy(mappingStrategy)
				.build();
		return csvToBean;
	}

}
