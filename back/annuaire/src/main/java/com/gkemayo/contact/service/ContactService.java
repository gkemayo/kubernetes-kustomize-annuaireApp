package com.gkemayo.contact.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.gkemayo.contact.repository.ContactRepository;
import com.gkemayo.contact.repository.entity.Contact;
import com.gkemayo.contact.service.dto.ContactDto;
import com.gkemayo.contact.service.exception.AnnuaryDataAccessException;
import com.gkemayo.contact.service.exception.ContactNotFoundException;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ContactService {
	
	@Autowired
	private ContactRepository contactRepository;
	
	/**
	 * Retourne un contact dans l'annuaire dont l'email correspond à celui passé en paramètre.
	 * @param email
	 * @return
	 */
	public ContactDto findContactByEmail(String email) throws ContactNotFoundException, AnnuaryDataAccessException {
		
		log.info("Entering findContactByEmail service");
		Contact contact = null;
		try {
			contact = contactRepository.findByEmailIgnoreCase(email);
		} catch (Exception e) {
			log.error("An error occurs when calling findContactByEmail {}", e);
			throw new AnnuaryDataAccessException("DataBaseAccesError", e.getMessage(), HttpStatus.SERVICE_UNAVAILABLE);
		}
		if(contact == null) {
			log.info("findContactByEmail - No content found with email {}", email);
			throw new ContactNotFoundException("ContactNotFound", "Contact not found", HttpStatus.NO_CONTENT);
		}
		return new ModelMapper().map(contact, ContactDto.class);
	}
	
	/**
	 * Retourne la liste de tous les contacts dans l'annuaire ayant le nom passé en paramètre
	 * @return
	 */
    public List<ContactDto> findByLastName(String lastName) throws AnnuaryDataAccessException {
    	
    	log.info("Entering findByLastName service");
		try {
			List<Contact> contactList = contactRepository.findByLastNameIgnoreCase(lastName);
			if(!CollectionUtils.isEmpty(contactList)) {
				return contactList.stream().map(c -> new ModelMapper().map(c, ContactDto.class)).collect(Collectors.toList());
			}
		} catch (Exception e) {
			log.error("An error occurs when calling findByLastName {}", e);
			throw new AnnuaryDataAccessException("DataBaseAccesError", e.getMessage(), HttpStatus.SERVICE_UNAVAILABLE);
		}
		return new ArrayList<ContactDto>();
	}

}
