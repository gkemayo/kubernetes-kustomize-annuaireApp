package com.gkemayo.contact.service.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContactNotFoundException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	
	private String errorCode;
	private HttpStatus httpStatus;
	
	public ContactNotFoundException(String errorCode, String message, HttpStatus httpStatus) {
		super(message);
		this.errorCode = errorCode;
		this.httpStatus = httpStatus;
	}

}
