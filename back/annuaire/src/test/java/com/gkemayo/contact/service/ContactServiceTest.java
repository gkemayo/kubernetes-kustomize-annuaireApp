package com.gkemayo.contact.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.gkemayo.contact.repository.ContactRepository;
import com.gkemayo.contact.repository.entity.Contact;
import com.gkemayo.contact.service.dto.ContactDto;
import com.gkemayo.contact.service.exception.AnnuaryDataAccessException;
import com.gkemayo.contact.service.exception.ContactNotFoundException;

@ExtendWith(SpringExtension.class)
public class ContactServiceTest {
	
	@Mock
	private ContactRepository contactRepository;
	
	@InjectMocks
	private ContactService contactService;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);
	}
	
	@Test
	public void testFindContactByEmail_with_content_ok() throws Exception {
		
		Contact contact = Contact.builder().id("6316868375").firstName("Leonardo").lastName("Amor")
				.email("lamor0@utexas.edu").company("Devshare").departement("Sales").build();
		
		when(contactRepository.findByEmailIgnoreCase(Mockito.anyString())).thenReturn(contact);

		ContactDto response = contactService.findContactByEmail("lamor0@utexas.edu");
		
		assertNotNull(response);
		assertEquals(contact.getId(), response.getId());
		assertEquals(contact.getEmail(), response.getEmail());
	}
	
	@Test
	public void testFindContactByEmail_no_content_NoContactFoundException() {
		
		Assertions.assertThrows(ContactNotFoundException.class, () -> {
			when(contactRepository.findByEmailIgnoreCase(Mockito.anyString())).thenReturn(null);
			contactService.findContactByEmail("lamor0@utexas.edu");
		});
	}

	@Test
	public void testFindContactByEmail_AnnuaryDataAccessException() {
		Assertions.assertThrows(AnnuaryDataAccessException.class, () -> {
			when(contactRepository.findByEmailIgnoreCase(Mockito.anyString())).thenThrow(new RuntimeException());
			contactService.findContactByEmail("lamor0@utexas.edu");
		});
	}
	
	
	//De même pour les tests de la méthode findByLastName

}
